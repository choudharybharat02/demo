import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingComponent } from './landing/landing.component';
import { TopicComponent } from './topic/topic.component';

const routes: Routes = [
  {
    path: '', component: LandingComponent
  },
  {
    path:'dashboard/:userId', component: DashboardComponent
  },
  {
    path:'topic/:userId/:topicId', component:TopicComponent
  },
  {
    path:'**', redirectTo:''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
