import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  username: string;
  constructor(private user: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  redirectToDashboard(){
    const users= this.user.getUsers() || [];
    let user= users.find(x=> x.username== this.username);
    if(!user){
      this.user.setUser(this.username);
      this.username='';
      this.router.navigate(['dashboard', users.length]);
    }else{
      this.router.navigate(['dashboard', user.userId]);
    }


    //redirecto
  }

}
