import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  topics:Array<any>;
  userId: string;
  constructor(private user: UserService, private router: Router, private route: ActivatedRoute) { 
    this.topics=[];
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(param=>{
      this.userId= param.get('userId');
      this.topics= this.user.getTopicById(this.userId);
    });
  }
  add(){
    this.router.navigate(['topic', this.userId, 0]);
  }
  back(){
    this.router.navigate(['']);
  }
  onTopicClick(topic){
    this.router.navigate(['topic', this.userId, topic.topicId]);
  }
}
