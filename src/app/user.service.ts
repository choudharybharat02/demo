import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUsers(){
    return JSON.parse(localStorage.getItem('users'));
  }
  
  getUserById(id: number){
    let users= this.getUsers();
    return users.filter((x=> x.id ==id));
  }

  setUser(name: string){
    let users= this.getUsers();
    if(!users){
      users=[{username: name, userId: 1}];
    }else{
      users.push({username: name, userId: users.length+1});
    }
    localStorage.setItem('users', JSON.stringify(users));
  }

  getTopics(){
    return JSON.parse(localStorage.getItem('topics'));
  }
  
  getTopicById(id: string){
    let topic= this.getTopics();
    return topic? topic.filter((x=> x.userId ==id)): [];
  }
  getTopicByTopicId(topicId: string){
    let topic= this.getTopics();
    return topic? topic.find((x=> x.topicId ==topicId)): [];
  }

  setTopic(topic: any){
    let topics= this.getTopics();
    if(!topics){
      topics=[{title: topic.title, userId: topic.userId, percentage: topic.percentage, topicId: topic.topicId, topicDetail: topic.topicDetail}];
    }else{
      topics.push({title: topic.title, userId: topic.userId, percentage: topic.percentage, topicId: topic.topicId, topicDetail: topic.topicDetail});
    }
    localStorage.setItem('topics', JSON.stringify(topics));
  }

  updateTopicById(userId, topicId, val){
    let topics= this.getTopicById(userId);
    const index= topics.findIndex(x=> x.topicId== topicId);
    if(index >=0){
      topics[index]=val;  
    }
    localStorage.setItem('topics', JSON.stringify(topics));
  }
}
