import { Template } from '@angular/compiler/src/render3/r3_ast';
import { AfterViewInit, Component, ComponentFactoryResolver, ElementRef, HostListener, OnInit, Renderer2, TemplateRef, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class TopicComponent implements OnInit, AfterViewInit {
  topic: string;
  title:string;
  @ViewChild('textarea') textarea: ElementRef<any>;
  userId: string;
  topicId: string;
  content: string;
  isTextAreaVisible: boolean;

  constructor(private user: UserService,
    private router: Router,
    private renderer2: Renderer2,
    private route: ActivatedRoute) {
    this.isTextAreaVisible=true;
   }

  ngOnInit(): void {
    this.route.paramMap.subscribe(param=>{
      this.userId= param.get('userId');
      this.topicId= param.get('topicId');
      const topic= this.user.getTopicByTopicId(this.topicId);
      if(topic){
        this.title= topic.title;
        this.content= topic.topicDetail;
      }
    });
  }
  ngAfterViewInit(): void{
    this.renderer2.listen(document, 'keyup', this.onEsc.bind(this));
    if(this.content){
      setTimeout(()=> {
        this.textarea.nativeElement.innerHTML= this.content;
        this.onEnter();
      },0)
      
    } 
  }

  saveTopic(){
    this.closeDropdown();
    const elems= document.querySelectorAll('#mainContainer span');
    let sum=0;
    const total= elems.length;
    elems.forEach(elm=>{
      switch (elm.className) {
        case 'done':
          sum+=4;
          break;
        case 'semi':
          sum+=3;
          break;
        case 'not-clear':
          sum+=2;
          break;
        case 'rubbish':
          sum+=1;
          break;
        default:
          break;
      }
    })
    const percentage=(sum/(total*4))*100;
    const topics= this.user.getTopicById(this.userId);
    let id= topics.length+1;
    if(Number(this.topicId)){
      let topic = this.user.getTopicByTopicId(this.topicId);
      if( topic) id=topic.topicId;
      this.topicId=id;
      this.user.updateTopicById(this.userId, this.topicId, {title: this.title, percentage: percentage, userId: this.userId, topicId:id , topicDetail: this.content})
      return;
    }
    this.topicId=id;
    this.user.setTopic({title: this.title, percentage: percentage, userId: this.userId, topicId:id , topicDetail: this.content})
  }
  back(){
    this.router.navigate(['dashboard', this.userId]);
  }

  onEnter(){
    const topics= this.textarea.nativeElement.innerText;
    this.content= "<span>"+topics.replace(/([-\,\(\)\[\]\'\"\\\\\/;:?{}.|]+)/g, '</span>$&<span>');
    setTimeout(() => {
      const elems= document.querySelectorAll('#mainContainer span');
    elems.forEach((elm)=>{
      elm.addEventListener('click', (e)=>{
        this.onClick(e);
      })
    });
    }, 0);
  }

  onClick(e){
    document.getElementById("myDropdown").classList.remove("show");
    document.getElementById("myDropdown").classList.add("show");
    document.getElementById("myDropdown").style.left=e.target.offsetLeft+ 'px';
    this.removeActiveSpan();
    e.target.classList.add('active');
  }

  onOptionClick(e, val){
    console.log(val);
    const activeElm= document.querySelector('.active');
    switch (val) {
      case 'UNDERSTOOD':
        activeElm.className='active done';
        break;
      case 'SOMEWHAT UNDERSTOOD':
        activeElm.className='active semi';
        break;
      case 'NOT CLEAR':
        activeElm.className='active not-clear';
        break;
      case 'WHAT RUBBISH':
        activeElm.className='active rubbish';
        break;
      default:
        break;
    }
    
  }
  
  onEsc(e){
    if(e.keyCode==27 || e.keyCode==13){
      this.closeDropdown();
    }
    
  }

  closeDropdown(){
    const elm=document.getElementById("myDropdown");
    if(elm) elm.classList.remove("show");
    this.removeActiveSpan();
  }

  removeActiveSpan(){
    const elems= document.querySelectorAll('#mainContainer span');
      elems.forEach(elm=>{
        elm.classList.remove('active');
      });
  }
  

}
